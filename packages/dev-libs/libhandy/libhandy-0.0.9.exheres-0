# Copyright 2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require meson \
    vala [ vala_dep=true with_opt=true ]

SUMMARY="Library with GTK+ widgets for mobile phones"
DESCRIPTION="
libhandy provides GTK+ widgets and GObjects to ease developing applications for mobile phones.
"
HOMEPAGE="https://source.puri.sm/Librem5/${PN}"
DOWNLOADS="${HOMEPAGE}/-/archive/v${PV}/${PN}-v${PV}.tar.bz2"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    glade [[ description = [ build and install glade catalog ] ]]
    gobject-introspection
    gtk-doc
    vapi [[ requires = [ gobject-introspection ] ]]
"

# require X access
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
        gtk-doc? ( dev-doc/gtk-doc )
    build+run:
        dev-libs/glib:2
        glade? ( dev-util/glade )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1 )
"

MESON_SOURCE=${WORKBASE}/${PN}-v${PV}

MESON_SRC_CONFIGURE_PARAMS=(
    -Dexamples=false
    -Dprofiling=false
    -Dstatic=false
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'glade glade_catalog'
    'gobject-introspection introspection'
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc gtk_doc'
    vapi
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

