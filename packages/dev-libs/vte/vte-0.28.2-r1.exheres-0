# Copyright (C) 2008 Stephen Bennett <spb@exherbo.org>
# Copyright (C) 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org

SUMMARY="A terminal emulator widget"
HOMEPAGE="http://developer.gnome.org/arch/gnome/widgets/vte.html"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="doc gobject-introspection python"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.35]
        doc? ( dev-doc/gtk-doc[>=1.0] )
        gobject-introspection? ( gnome-desktop/gobject-introspection[>=0.6.7] )
        virtual/pkg-config[>=0.19]
    build+run:
        dev-libs/glib:2[>=2.22.0]
        x11-libs/gtk+:2[>=2.20.0]
        x11-libs/pango[>=1.22.0]
        sys-libs/ncurses
        python? ( gnome-bindings/pygtk:2[>=2.4] )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    -p0 "${FILES}"/${PNV}-alt-key.patch
    -p0 "${FILES}"/${PNV}-musl.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=( --disable-deprecation '--with-gtk=2.0' )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'doc gtk-doc' 'gobject-introspection introspection' 'python' )

src_prepare() {
    default
    # Respect datarootdir
    edo sed -e 's:itlocaledir = $(prefix)/$(DATADIRNAME)/locale:itlocaledir = $(datarootdir)/locale:' \
        -i "${WORK}"/po/Makefile.in.in
}

