# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Purpose License v2

require gnome.org [ suffix='tar.xz' ] gsettings gtk-icon-cache freedesktop-desktop

SUMMARY="IM client for GNOME"
HOMEPAGE="https://live.gnome.org/Empathy"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    cheese          [[ description = [ Enable webcam support for avatars ] ]]
    geolocation     [[ description = [ Displays your buddies on a map and transmit your position ] ]]
    spell
    ( linguas: an ar as ast az be be@latin bg bn bn_IN br ca ca@valencia crh cs cy da de dz el en_CA
               en_GB en@shaw eo es et eu fa fi fr ga gl gu he hi hr hu id it ja kk km kn ko ku lt lv
               mai mk ml mr ms my nb ne nl nn oc or pa pl ps pt pt_BR ro ru si sk sl sq sr sr@latin
               sv ta te tg th tr ug uk vi xh zh_CN zh_HK zh_TW zu )
"

DEPENDENCIES="
    build:
        dev-libs/libxslt
        dev-util/intltool[>=0.50.0]
        dev-util/itstool [[ note = [ from YELP_HELP_INIT in configure.ac ] ]]
        dev-lang/python:2.7
        gnome-desktop/yelp-tools
        sys-devel/gettext
        virtual/pkg-config[>=0.25]
    build+run:
        base/libgee:0.8
        dev-libs/dbus-glib:1
        dev-libs/glib:2[>=2.48]
        dev-libs/libsecret:1[>=0.5]
        dev-libs/libxml2:2.0
        gnome-desktop/gcr[>=2.91.4]
        gnome-desktop/gnome-online-accounts
        gnome-desktop/gsettings-desktop-schemas
        gnome-desktop/libgudev
        gnome-desktop/libsoup:2.4
        media-libs/clutter-gst:3.0
        media-libs/gstreamer:1.0[>=0.10.32]
        media-libs/libcanberra[>=0.25][providers:gtk3]
        media-plugins/gst-plugins-base:1.0
        media-sound/pulseaudio
        net-im/farstream:0.2
        net-im/folks[>=0.9.5][telepathy]
        net-im/telepathy-farstream[>=0.6.0]
        net-im/telepathy-glib[>=0.23.2]
        net-im/telepathy-logger[>=0.8]
        net-im/telepathy-mission-control
        net-libs/webkit:4.0[>=2.10.0]
        x11-libs/clutter:1[>=1.10]
        x11-libs/clutter-gtk:1.0[>=1.1.2]
        x11-libs/cogl:1.0[>=1.14]
        x11-libs/gtk+:3[>=3.9.4]
        x11-libs/libnotify[>=0.7]
        x11-libs/libX11
        cheese? ( gnome-desktop/cheese[>=3.4] )
        geolocation? (
            gnome-desktop/geocode-glib:1.0[>=0.99.1]
            gps/geoclue:2.0[>=2.1.0]
            x11-libs/libchamplain:0.12
        )
        spell? (
            app-spell/enchant:0[>=1.2.0]
            app-text/iso-codes[>=0.35]
        )
    suggestion:
        net-im/telepathy-gabble[>=0.16.0] [[ description = [ Support for XMPP alias Jabber ] ]]
        net-im/telepathy-haze[>=0.6.0] [[
            description = [ Support for several IM protocols via libpurble ]
        ]]
        net-im/telepathy-idle [[ description = [ IRC support ] ]]
        net-im/telepathy-salut[>=0.8.0] [[
            description = [ Chat with peoples in the same network via avahi ]
        ]]
"

# Requires X
RESTRICT="test"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-ubuntu-online-accounts
    --enable-goa
    --enable-gudev
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'geolocation map'
    'geolocation location'
    'geolocation geocode'
    'spell'
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( 'cheese' )

pkg_postinst() {
    gsettings_pkg_postinst
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    gsettings_pkg_postrm
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

